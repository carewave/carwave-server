                                                   **Volme Social**
                                                   
The project represents the OAuth2 implementation of the gaining authentication with a third-party authentication 
provider (Facebook, Google, Github, etc.)

Database container instantiation command sample: 
docker run --name volme_social_auth -d -p 1927:3306 -e MYSQL_DATABASE=spring_social -e MYSQL_ROOT_PASSWORD=volme mysql:5.7

------------------------------------------------------------------------------------------------------------------------

                                                        TESTING
                                                        
------------------------------------------------------------------------------------------------------------------------
To test the solution the following steps are required:

1. Make a call to http://localhost:8080/oauth2/authorize/google?redirect_uri=http://localhost:1908/

2. In the response url you'll have 'token' request parameter. Example: http://localhost:1908/?token=*******

3. Copy the token and try to access some protected resource of mock-api-gateway with Bearer token Authorization. 
Example: http://localhost:1908/role-user Authorization Bearer *token received in step 2*.

Also, you can authorize in an old-fashioned way - by providing username and password. See AuthController class for more
details.
