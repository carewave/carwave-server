package com.volme.volmesocialauth.model;

public enum AuthProvider {

    local,
    facebook,
    google,
    github
}
