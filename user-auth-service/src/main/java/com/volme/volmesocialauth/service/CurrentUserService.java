package com.volme.volmesocialauth.service;

import com.volme.volmesocialauth.model.User;
import com.volme.volmesocialauth.security.UserPrincipal;

public interface CurrentUserService {

    User getCurrentUser(UserPrincipal userPrincipal);
}
