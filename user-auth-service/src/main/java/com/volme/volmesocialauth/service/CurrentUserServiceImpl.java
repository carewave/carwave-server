package com.volme.volmesocialauth.service;

import com.volme.volmesocialauth.exception.ResourceNotFoundException;
import com.volme.volmesocialauth.model.User;
import com.volme.volmesocialauth.repository.UserRepository;
import com.volme.volmesocialauth.security.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrentUserServiceImpl implements CurrentUserService {

    private UserRepository userRepository;

    @Autowired
    public CurrentUserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getCurrentUser(UserPrincipal userPrincipal) {
        return userRepository.findById(userPrincipal.getId())
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userPrincipal.getId()));

    }
}
