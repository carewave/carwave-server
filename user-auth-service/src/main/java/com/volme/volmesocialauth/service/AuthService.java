package com.volme.volmesocialauth.service;

import com.volme.volmesocialauth.dto.rest.LoginDto;
import com.volme.volmesocialauth.dto.rest.UserDto;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.net.URI;

public interface AuthService {

    String authenticateUser(@Valid @RequestBody LoginDto loginDto);

    URI registerUser(@Valid @RequestBody UserDto signUpRequest);

    URI updateUser(@Valid @RequestBody UserDto signUpRequest);
}
