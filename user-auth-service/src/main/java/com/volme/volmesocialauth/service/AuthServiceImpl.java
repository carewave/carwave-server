package com.volme.volmesocialauth.service;

import com.volme.volmesocialauth.dto.rest.LoginDto;
import com.volme.volmesocialauth.dto.rest.UserDto;
import com.volme.volmesocialauth.exception.BadRequestException;
import com.volme.volmesocialauth.model.AuthProvider;
import com.volme.volmesocialauth.model.User;
import com.volme.volmesocialauth.repository.UserRepository;
import com.volme.volmesocialauth.security.token.TokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@Service
public class AuthServiceImpl implements AuthService {

    private TokenProvider tokenProvider;
    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private static final String DEFAULT_CURRENT_USER_PATH = "/user/me";

    @Autowired
    public AuthServiceImpl(TokenProvider tokenProvider, AuthenticationManager authenticationManager,
                           UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public String authenticateUser(LoginDto loginDto) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDto.getEmail(),
                        loginDto.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.createToken(authentication);
    }

    @Override
    public URI registerUser(UserDto userDto) {
        if (userRepository.existsByEmail(userDto.getEmail())) {
            throw new BadRequestException("Email address already in use.");
        }

        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setProvider(AuthProvider.local);
        user.setEmailVerified(false);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        User result = userRepository.save(user);

        return ServletUriComponentsBuilder
                .fromCurrentContextPath().path(DEFAULT_CURRENT_USER_PATH)
                .buildAndExpand(result.getId()).toUri();

    }

    @Override
    public URI updateUser(UserDto userDto) {
        String requestUserEmail = userDto.getEmail();
        Optional<User> foundUser = userRepository.findByEmail(requestUserEmail);

        URI uri;
        if(foundUser.isPresent()){
            User user = foundUser.get();
            user.setName(userDto.getName());
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
            User result = userRepository.save(user);
            uri = ServletUriComponentsBuilder
                    .fromCurrentContextPath().path(DEFAULT_CURRENT_USER_PATH)
                    .buildAndExpand(result.getId()).toUri();
        } else {
            throw new BadRequestException("User update failed: email does not exist");
        }
        return uri;
    }
}