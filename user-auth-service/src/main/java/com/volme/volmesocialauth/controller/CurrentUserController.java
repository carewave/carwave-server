package com.volme.volmesocialauth.controller;

import com.volme.volmesocialauth.model.User;
import com.volme.volmesocialauth.security.CurrentUser;
import com.volme.volmesocialauth.security.UserPrincipal;
import com.volme.volmesocialauth.service.CurrentUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CurrentUserController {

    private CurrentUserService currentUserService;

    @Autowired
    public CurrentUserController(CurrentUserService currentUserService) {
        this.currentUserService = currentUserService;
    }

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    public User getCurrentUser(@CurrentUser UserPrincipal userPrincipal) {
        return currentUserService.getCurrentUser(userPrincipal);
    }
}