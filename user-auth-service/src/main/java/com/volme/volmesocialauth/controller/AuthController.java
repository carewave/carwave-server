package com.volme.volmesocialauth.controller;

import com.volme.volmesocialauth.dto.rest.ApiResponse;
import com.volme.volmesocialauth.dto.rest.AuthResponse;
import com.volme.volmesocialauth.dto.rest.LoginDto;
import com.volme.volmesocialauth.dto.rest.UserDto;
import com.volme.volmesocialauth.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        return ResponseEntity.ok(new AuthResponse(authService.authenticateUser(loginDto)));
    }

    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody UserDto userDto) {
        return ResponseEntity.created(authService.registerUser(userDto))
                .body(new ApiResponse(true, "User registered successfully!"));
    }

    @PutMapping("/update")
    public ResponseEntity<ApiResponse> updateUser(@Valid @RequestBody UserDto userDto) {
        return ResponseEntity.created(authService.updateUser(userDto))
                .body(new ApiResponse(true, "User is updated successfully!"));
    }

}