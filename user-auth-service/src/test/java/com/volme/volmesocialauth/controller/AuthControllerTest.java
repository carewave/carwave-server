package com.volme.volmesocialauth.controller;

import com.volme.volmesocialauth.dto.rest.ApiResponse;
import com.volme.volmesocialauth.dto.rest.UserDto;
import com.volme.volmesocialauth.exception.BadRequestException;
import com.volme.volmesocialauth.service.AuthService;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthControllerTest {

    @InjectMocks
    private AuthController subject;

    @Mock
    private AuthService authService;

    @Test
    public void shouldUpdateUser() {
        //given
        final UserDto userDto = UserDto.builder()
                .email(RandomStringUtils.randomNumeric(10))
                .name(RandomStringUtils.randomNumeric(10))
                .password(RandomStringUtils.randomNumeric(10))
                .build();

        final URI uriResponse = URI.create("") ;
        final ResponseEntity<ApiResponse> expected = ResponseEntity.created(uriResponse)
                .body(new ApiResponse(true, "User is updated successfully!"));

        doReturn(uriResponse).when(authService).updateUser(userDto);

        //when
        ResponseEntity<?> actual = subject.updateUser(userDto);

        //then
        verify(authService, times(1)).updateUser(userDto);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test(expected = BadRequestException.class)
    public void shouldNotUpdateUser() {
        //given
        final UserDto userDto = UserDto.builder()
                .email(RandomStringUtils.randomNumeric(10))
                .name(RandomStringUtils.randomNumeric(10))
                .password(RandomStringUtils.randomNumeric(10))
                .build();

        doThrow(BadRequestException.class)
                .when(authService)
                .updateUser(userDto);

        //when
        subject.updateUser(userDto);
    }

}
