package com.volme.gateway.resource.model;

public enum AuthProvider {
    local,
    facebook,
    google,
    github
}
