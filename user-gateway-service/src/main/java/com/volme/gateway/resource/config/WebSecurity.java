package com.volme.gateway.resource.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import com.volme.gateway.resource.security.token.TokenAuthenticationFilter;
import com.volme.gateway.resource.security.token.TokenProvider;

import javax.servlet.http.HttpServletResponse;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                // make sure we use stateless session; session won't be used to store user's state.
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                // handle an authorized attempts
                .exceptionHandling().authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse
                .SC_UNAUTHORIZED))
                .and()
                // Add a filter to validate the tokens with every request
                .addFilterAfter(new TokenAuthenticationFilter(tokenProvider()), UsernamePasswordAuthenticationFilter
                        .class)
                // authorization requests config
                .authorizeRequests()
                .antMatchers("/auth/**", "/oauth2/**").permitAll()
                .anyRequest().authenticated();
    }

    @Bean
    public AppProperties appProperties() {
        return new AppProperties();
    }

    @Bean
    public TokenProvider tokenProvider() {
        return new TokenProvider(appProperties());
    }

}
