package com.volme.gateway.resource.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//For test papooses only!!!
@RestController
public class Controller {
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping("/role-admin")
    public String getResource() {
        return "Hello from ADMIN!";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @GetMapping("/role-user")
    public String getRoleUser() {
        return "Hello from USER!";
    }
}

