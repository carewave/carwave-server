                                                **Volme USER-GATEWAY-SERVICE**
                                                
The module performs as a gateway for the user-related(mobile apps) HTTP calls. 

It mostly intended for the authorization calls: to validate JWT tokens. It also redirects to the user authorization 
server. Once token's signature recognized as a valid one - the authentication context is filed by a corresponding 
Authentication object with the following claims: email from the token and a default "USER_ROLE" user.
 