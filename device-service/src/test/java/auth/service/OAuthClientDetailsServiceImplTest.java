package auth.service;

import auth.model.OauthClientDetails;
import auth.dto.rest.DeviceDto;
import auth.persistance.OAuthClientDetailsDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.Silent.class)
public class OAuthClientDetailsServiceImplTest {

    @Mock
    private OAuthClientDetailsDao clientDetailsDao;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private OAuthClientDetailsServiceImpl clientDetailsService;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(clientDetailsService, "resourceIds", "api-gateway");
        ReflectionTestUtils.setField(clientDetailsService, "authorities", "PI_DEVICE_ADMIN");
        ReflectionTestUtils.setField(clientDetailsService, "scope", "read, write");
        ReflectionTestUtils.setField(clientDetailsService, "tokenValidity", 10800);
    }

    @Test
    public void registerNewDevice_shouldCreateNewOAuthClientDetailsObjectWithExternalIdAndPresettedDefaults() {
        //given
        String deviceId = "1927";
        String resourceId = "api-gateway";
        String password = "password";
        String clientSecret = "encoded_password";
        OauthClientDetails clientDetails = new OauthClientDetails();
        clientDetails.setClientId(deviceId);
        clientDetails.setClientSecret(clientSecret);
        clientDetails.setResourceIds(resourceId);
        clientDetails.setScope("read, write");
        clientDetails.setAuthorizedGrantTypes("client_credentials");
        clientDetails.setAuthorities("PI_DEVICE_ADMIN");
        clientDetails.setAccessTokenValidity(10800);
        DeviceDto deviceDto = new DeviceDto(deviceId, password);
        when(passwordEncoder.encode(password)).thenReturn(clientSecret);
        when(clientDetailsDao.save(clientDetails)).thenReturn(clientDetails);
        //when
        OauthClientDetails actual = clientDetailsService.registerNewDevice(deviceDto);
        //then
        assertThat(actual, is(equalTo(clientDetails)));
        verify(clientDetailsDao, times(1)).save(clientDetails);
    }

}