package auth.control;

import auth.dto.data.OauthClientDetailsDto;
import auth.dto.rest.DeviceDto;
import auth.service.OAuthClientDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("register")
public class DeviceRegistartionController {

    private OAuthClientDetailsService clientDetailsService;

    @Autowired
    public DeviceRegistartionController(OAuthClientDetailsService clientDetailsService) {
        this.clientDetailsService = clientDetailsService;
    }

    @PostMapping(value = "/device-registration", produces = "application/json; charset=utf-8")
    ResponseEntity<OauthClientDetailsDto> registerDevice(@RequestBody DeviceDto deviceDto) {
        OauthClientDetailsDto response = OauthClientDetailsDto.parseClientDetails(clientDetailsService
                .registerNewDevice(deviceDto));
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
