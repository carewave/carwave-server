package auth.persistance;

import auth.model.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OAuthClientDetailsDao extends JpaRepository<OauthClientDetails, Integer> {

    @Override
    <S extends OauthClientDetails> S save(S s);
}
