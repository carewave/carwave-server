package auth.dto.rest;

public class DeviceDto {

    private String deviceId;
    private String password;

    // Added an empty constructor to satisfy the Jackson Mapper: @RequestBody fails to map received JSON to the DTO
    // without explicitly declaring the constructor.
    public DeviceDto() {

    }

    public DeviceDto(String deviceId, String password) {
        this.deviceId = deviceId;
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getPassword() {
        return password;
    }

}
