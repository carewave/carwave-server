package auth.dto.data;

import auth.model.OauthClientDetails;
import lombok.Data;

@Data
public class OauthClientDetailsDto {
    private String clientId;
    private String scope;
    private String resourceIds;
    private String authorizationGrantTypes;
    private String authorities;

    public static OauthClientDetailsDto parseClientDetails(OauthClientDetails clientDetails) {
        //TODO: Implement builder pattern
        OauthClientDetailsDto clientDetailsDto = new OauthClientDetailsDto();
        clientDetailsDto.setClientId(clientDetails.getClientId());
        clientDetailsDto.setScope(clientDetails.getScope());
        clientDetailsDto.setResourceIds(clientDetails.getResourceIds());
        clientDetailsDto.setAuthorizationGrantTypes(clientDetails.getAuthorizedGrantTypes());
        clientDetailsDto.setAuthorities(clientDetails.getAuthorities());
        return clientDetailsDto;
    }
}
