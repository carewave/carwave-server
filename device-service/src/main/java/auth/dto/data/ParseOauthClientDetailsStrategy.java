package auth.dto.data;

import auth.model.OauthClientDetails;

public interface ParseOauthClientDetailsStrategy {
    void parseClientDetails(OauthClientDetails clientDetails,
                            OauthClientDetailsDto clientDetailsDto);

}
