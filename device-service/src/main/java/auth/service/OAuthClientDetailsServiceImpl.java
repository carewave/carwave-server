package auth.service;

import auth.dto.rest.DeviceDto;
import auth.model.OauthClientDetails;
import auth.persistance.OAuthClientDetailsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@PropertySource(value = "classpath:bootstrap.yml")
@Transactional
public class OAuthClientDetailsServiceImpl implements OAuthClientDetailsService {

    private static final String CLIENT_CREDENTIALS_GRANT_TYPE = "client_credentials";
    private static final String CLIENT_NAME = "PI_DEVICE";
    private PasswordEncoder passwordEncoder;
    private OAuthClientDetailsDao clientDetailsDao;

    @Value("${resource.ids:api-gateway}")
    private String resourceIds;

    @Value("${device.token.authorities:PI_DEVICE_ADMIN}")
    private String authorities;

    @Value("${device.token.scope:read, write}")
    private String scope;

    @Value("${device.token.validity:10800}")
    private int tokenValidity;

    @Autowired
    public OAuthClientDetailsServiceImpl(PasswordEncoder passwordEncoder, OAuthClientDetailsDao
            clientDetailsDao) {
        this.passwordEncoder = passwordEncoder;
        this.clientDetailsDao = clientDetailsDao;
    }

    //TODO: Wrap the OauthClientDetails return object to a DTO object to return from the controller level.
    @Override
    public OauthClientDetails registerNewDevice(DeviceDto deviceDto) {
        OauthClientDetails device = new OauthClientDetails();
        device.setClientId(deviceDto.getDeviceId());
        device.setClientSecret(passwordEncoder.encode(deviceDto.getPassword()));
        device.setResourceIds(resourceIds);
        device.setScope(scope);
        device.setAuthorizedGrantTypes(CLIENT_CREDENTIALS_GRANT_TYPE);
        device.setAuthorities(authorities);
        device.setCreated(new Date());
        device.setClientName(CLIENT_NAME);
        device.setEnabled(true);
        device.setAccessTokenValidity(tokenValidity);
        OauthClientDetails response = clientDetailsDao.save(device);
        return response;
    }
}
