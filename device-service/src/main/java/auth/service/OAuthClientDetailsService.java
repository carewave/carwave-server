package auth.service;

import auth.model.OauthClientDetails;
import auth.dto.rest.DeviceDto;

public interface OAuthClientDetailsService {

    OauthClientDetails registerNewDevice(DeviceDto deviceDto);
}
