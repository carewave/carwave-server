package com.volme.pairing.dao;

import com.volme.pairing.exception.ObjectNotFoundError;
import com.volme.pairing.exception.PairDaoException;
import com.volme.pairing.model.PairEntity;
import com.volme.pairing.repository.PairEntityRepository;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PairDaoImplTest {

    @Mock
    private PairEntityRepository pairEntityRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @InjectMocks
    public PairDaoImpl subject;

    @Test
    public void save_shouldVerifyRepositoryMethodUsage() {
        //given
        PairEntity entity = new PairEntity();
        doReturn(entity).when(pairEntityRepository).save(entity);
        //when
        subject.save(entity);
        //then
        verify(pairEntityRepository, times(1)).save(entity);

    }

    @Test
    public void findByEmailAndDeviceId_shouldVerifyRepositoryMethodUsage() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        PairEntity entity = new PairEntity();
        doReturn(Optional.of(entity)).when(pairEntityRepository).findByEmailAndDeviceId(email, deviceId);
        //when
        subject.findByEmailAndDeviceId(email, deviceId);
        //then
        verify(pairEntityRepository, times(2)).findByEmailAndDeviceId(email, deviceId);
    }

    @Test
    public void findByEmailAndDeviceId_shouldThrowExceptionIfNotFound() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        doReturn(Optional.empty()).when(pairEntityRepository).findByEmailAndDeviceId(email, deviceId);
        thrown.expect(ObjectNotFoundError.class);
        thrown.expectMessage("There is no pair with email 'email@volme.com' and device id '1927'.");
        //when
        subject.findByEmailAndDeviceId(email, deviceId);
        //then exception is thrown
    }

    @Test
    public void findByDeviceId_shouldVerifyRepositoryMethodUsage() {
        //given
        String deviceId = "1927";
        PairEntity entity = new PairEntity();
        doReturn(Collections.singletonList(entity))
                .when(pairEntityRepository).findAllByDeviceId(deviceId);
        //when
        subject.findAllByDeviceId(deviceId);
        //then
        verify(pairEntityRepository, times(1)).findAllByDeviceId(deviceId);
    }

    @Test
    public void update_shouldVerifyRepositoryMethodUsage() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        doReturn(1).when(pairEntityRepository).update(true, email, deviceId);
        //when
        subject.update(true, email, deviceId);
        //then
        verify(pairEntityRepository, times(1)).update(true, email, deviceId);
    }

    @Test
    public void update_shouldThrowExceptionWhenNoRowsAffected() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        doReturn(0).when(pairEntityRepository).update(true, email, deviceId);
        thrown.expect(PairDaoException.class);
        thrown.expectMessage("'update' operation error - 0 rows were affected!");
        //when
        subject.update(true, email, deviceId);
        //then exception is thrown
    }

    @Test
    public void findAllPairs_shouldVerifyRepositoryMethodUsage() {
        //given
        doReturn(Collections.EMPTY_LIST).when(pairEntityRepository).findAll();
        //when
        subject.findAllPairs();
        //then
        verify(pairEntityRepository, times(1)).findAll();
    }
}