package com.volme.pairing.service;

import com.volme.pairing.dao.PairDao;
import com.volme.pairing.dto.rest.request.PairRequest;
import com.volme.pairing.dto.rest.response.PairResponse;
import com.volme.pairing.exception.ObjectNotFoundError;
import com.volme.pairing.model.PairEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PairingServiceImplTest {
    @Mock
    private PairDao pairDao;

    @InjectMocks
    private PairingServiceImpl subject;

    @Test
    public void pair_shouldCallDaoSaveMethodForEntityDoesntExist() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairEntity pairEntity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress, true);
        doThrow(new ObjectNotFoundError(String.format(
                "There is no pair with email '%s' and device id '%s'.", email,
                deviceId))).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        doReturn(pairEntity).when(pairDao).save(pairEntity);
        //when
        subject.pair(request);
        //then
        verify(pairDao, times(1)).findByEmailAndDeviceId(email, deviceId);
        verify(pairDao, times(1)).save(pairEntity);
    }

    @Test
    public void pair_shouldCallUpdateDaoMethodForUnpairedEntity() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairEntity unpairedEntity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress,
                false);
        doReturn(unpairedEntity).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        //when
        subject.pair(request);
        //then
        verify(pairDao, times(2)).findByEmailAndDeviceId(email, deviceId);
        verify(pairDao, times(1)).update(true, email, deviceId);
    }

    @Test
    public void pair_shouldReturnPairedResponseWithWasPairedTrueForAlreadyPairedEntity() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairEntity entity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress, true);
        doReturn(entity).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        PairResponse expected = new PairResponse(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress,
                true, true);
        //when
        PairResponse actual = subject.pair(request);
        //then
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void unpair_shouldCallDaoUpdateMethodForExistingPairedEntity() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairEntity entity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress, true);
        doReturn(entity).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        //when
        PairResponse actual = subject.unpair(request);
        //then
        verify(pairDao, times(2)).findByEmailAndDeviceId(email, deviceId);
        verify(pairDao, times(1)).update(false, email, deviceId);
    }

    @Test
    public void unpair_shouldReturnPairedResponseWithWasPairedTrueForAlreadyUnpairedEntity() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairEntity entity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress, false);
        doReturn(entity).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        PairResponse expected = new PairResponse(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress,
                false, true);
        //when
        PairResponse actual = subject.unpair(request);
        //then
        verify(pairDao, times(1)).findByEmailAndDeviceId(email, deviceId);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void getPairByEmailAndDeviceId_shouldCallDaoFindByEmailAndDeviceId() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairEntity entity = new PairEntity(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress, true);
        doReturn(entity).when(pairDao).findByEmailAndDeviceId(email, deviceId);
        //when
        subject.getPairByEmailAndDeviceId(email, deviceId);
        //then
        verify(pairDao, times(1)).findByEmailAndDeviceId(email, deviceId);
    }

    @Test
    public void getAllPairs_shouldCallDaoFindAllPairsAndConvertReturnedResultToList() {
        //given
        String email1 = "email@volme.com";
        String deviceId1 = "1927";
        String fcmPushNotificationToken1 = "token1";
        String bluetoothMacAddress1 = "00:00:00:00:00:11";
        String email2 = "email2@volme.com";
        String deviceId2 = "1488";
        String fcmPushNotificationToken2 = "token2";
        String bluetoothMacAddress2 = "00:00:00:00:00:22";
        String email3 = "email3@volme.com";
        String deviceId3 = "1967";
        String fcmPushNotificationToken3 = "token3";
        String bluetoothMacAddress3 = "00:00:00:00:00:33";
        Collection<PairEntity> entities = Arrays.asList(
                new PairEntity(email1, deviceId1, fcmPushNotificationToken1, bluetoothMacAddress1, true),
                new PairEntity(email2, deviceId2, fcmPushNotificationToken2, bluetoothMacAddress2, false),
                new PairEntity(email3, deviceId3, fcmPushNotificationToken3, bluetoothMacAddress3, true));
        List<PairResponse> expected = Arrays.asList(
                new PairResponse(email1, deviceId1, fcmPushNotificationToken1, bluetoothMacAddress1, true),
                new PairResponse(email2, deviceId2, fcmPushNotificationToken2, bluetoothMacAddress2, false),
                new PairResponse(email3, deviceId3, fcmPushNotificationToken3, bluetoothMacAddress3, true));
        doReturn(entities).when(pairDao).findAllPairs();
        //when
        List<PairResponse> actual = subject.getAllPairs();
        //then
        verify(pairDao, times(1)).findAllPairs();
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void getPairsByDeviceId_shouldCallDaoFindAllByDeviceIdAndConvertReturnedResultToListOfPairResponse() {
        //given
        String deviceId = "1927";
        String email1 = "email@volme.com";
        String fcmPushNotificationToken1 = "token1";
        String bluetoothMacAddress1 = "00:00:00:00:00:11";
        String email2 = "email2@volme.com";
        String fcmPushNotificationToken2 = "token2";
        String bluetoothMacAddress2 = "00:00:00:00:00:22";
        String email3 = "email3@volme.com";
        String fcmPushNotificationToken3 = "token3";
        String bluetoothMacAddress3 = "00:00:00:00:00:33";

        List<PairEntity> entities = Arrays.asList(
                new PairEntity(email1, deviceId, fcmPushNotificationToken1, bluetoothMacAddress1, true),
                new PairEntity(email2, deviceId, fcmPushNotificationToken2, bluetoothMacAddress2, false),
                new PairEntity(email3, deviceId, fcmPushNotificationToken3, bluetoothMacAddress3, true));
        List<PairResponse> expected = Arrays.asList(
                new PairResponse(email1, deviceId, fcmPushNotificationToken1, bluetoothMacAddress1, true),
                new PairResponse(email3, deviceId, fcmPushNotificationToken3, bluetoothMacAddress3, true));
        doReturn(entities).when(pairDao).findAllByDeviceId(deviceId);
        //when
        List<PairResponse> actual = subject.getPairedPairsByDeviceId(deviceId);
        //then
        verify(pairDao, times(1)).findAllByDeviceId(deviceId);
        assertThat(actual, is(equalTo(expected)));
    }
}