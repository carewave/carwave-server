package com.volme.pairing.controller;

import com.volme.pairing.dto.rest.request.PairRequest;
import com.volme.pairing.dto.rest.response.PairResponse;
import com.volme.pairing.service.PairingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PairControllerTest {
    @Mock
    private PairingService pairingService;

    @InjectMocks
    private PairController subject;

    @Test
    public void pair_shouldCallServiceMethodAndWrapResponseToResponseEntityCreatedForUnpairedOrNewPair() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairResponse responseFromPairingService = new PairResponse(email, deviceId, fcmPushNotificationToken,
                bluetoothMacAddress, true);
        ResponseEntity<PairResponse> expected = new ResponseEntity<>(responseFromPairingService, HttpStatus.CREATED);
        doReturn(responseFromPairingService).when(pairingService).pair(request);
        //when
        ResponseEntity<PairResponse> actual = subject.pair(request);
        //then
        verify(pairingService, times(1)).pair(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void pair_shouldCallServiceMethodAndWrapResponseToResponseEntityConflictForAlreadyPairedPair() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairResponse responseFromPairingService = new PairResponse(email, deviceId, fcmPushNotificationToken,
                bluetoothMacAddress, true, true);
        ResponseEntity<PairResponse> expected = new ResponseEntity<>(responseFromPairingService, HttpStatus.CONFLICT);
        doReturn(responseFromPairingService).when(pairingService).pair(request);
        //when
        ResponseEntity<PairResponse> actual = subject.pair(request);
        //then
        verify(pairingService, times(1)).pair(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void unpair_shouldCallServiceMethodAndWrapResponseToResponseEntityCreatedForExistingAndPairedPair() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairResponse responseFromPairingService = new PairResponse(email, deviceId, fcmPushNotificationToken,
                bluetoothMacAddress, true, false);
        ResponseEntity<PairResponse> expected = new ResponseEntity<>(responseFromPairingService, HttpStatus.CREATED);
        doReturn(responseFromPairingService).when(pairingService).unpair(request);
        //when
        ResponseEntity<PairResponse> actual = subject.unpair(request);
        //then
        verify(pairingService, times(1)).unpair(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void unpair_shouldCallServiceMethodAndWrapResponseToResponseEntityConflictForAlreadyUnpairedPair() {
        //given
        String email = "email@volme.com";
        String deviceId = "1927";
        String fcmPushNotificationToken = "token";
        String bluetoothMacAddress = "00:00:00:00:00:00";
        PairRequest request = new PairRequest(email, deviceId, fcmPushNotificationToken, bluetoothMacAddress);
        PairResponse responseFromPairingService = new PairResponse(email, deviceId, fcmPushNotificationToken,
                bluetoothMacAddress, true, true);
        ResponseEntity<PairResponse> expected = new ResponseEntity<>(responseFromPairingService, HttpStatus.CONFLICT);
        doReturn(responseFromPairingService).when(pairingService).unpair(request);
        //when
        ResponseEntity<PairResponse> actual = subject.unpair(request);
        //then
        verify(pairingService, times(1)).unpair(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void getAllPairs_shouldCallServiceMethodAndWrapResponseToResponseEntityOk() {
        //given
        List<PairResponse> responseFromPairService = Arrays.asList(
                new PairResponse(
                        "email@volme.com", "1927", "token",
                        "00:00:00:00:00:00", true
                ),
                new PairResponse(
                        "email2@volme.com", "1488", "token1",
                        "11:11:11:11:11:11", true)
        );
        ResponseEntity<List<PairResponse>> expected = new ResponseEntity<>(responseFromPairService, HttpStatus.OK);
        doReturn(responseFromPairService).when(pairingService).getAllPairs();
        //when
        ResponseEntity<List<PairResponse>> actual = subject.getAllPairs();
        //then
        verify(pairingService, times(1)).getAllPairs();
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void getAllPairsByDeviceId_shouldCallServiceMethodAndWrapResponseToResponseEntityOk() {
        //given
        String deviceId = "1927";
        List<PairResponse> responseFromPairService = Arrays.asList(
                new PairResponse(
                        "email@volme.com", "1927", "token"
                        , "00:00:00:00:00:00", true),
                new PairResponse("email2@volme.com", "1488", "token1"
                        , "11:11:11:11:11:11", true)
        );
        ResponseEntity<List<PairResponse>> expected = new ResponseEntity<>(responseFromPairService, HttpStatus.OK);
        doReturn(responseFromPairService).when(pairingService).getPairedPairsByDeviceId(deviceId);
        //when
        ResponseEntity<List<PairResponse>> actual = subject.getAllPairsByDeviceId(deviceId);
        //then
        verify(pairingService, times(1)).getPairedPairsByDeviceId(deviceId);
        assertThat(actual, is(equalTo(expected)));

    }

}