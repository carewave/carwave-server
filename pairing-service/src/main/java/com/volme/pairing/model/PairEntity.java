package com.volme.pairing.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "pairs",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"email", "deviceId"})})
public final class PairEntity {
    @Id
    @GeneratedValue(
            strategy = GenerationType.AUTO,
            generator = "native"
    )
    @GenericGenerator(
            name = "native",
            strategy = "native"
    )
    private Long id;
    private String email;
    private String deviceId;
    private String fcmPushNotificationToken;
    private String bluetoothMacAddress;
    private boolean paired;

    public PairEntity() {

    }

    public PairEntity(String email, String deviceId, String fcmPushNotificationToken, String bluetoothMacAddress,
                      boolean paired) {
        this.email = email;
        this.deviceId = deviceId;
        this.fcmPushNotificationToken = fcmPushNotificationToken;
        this.bluetoothMacAddress = bluetoothMacAddress;
        this.paired = paired;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFcmPushNotificationToken() {
        return fcmPushNotificationToken;
    }

    public void setFcmPushNotificationToken(String fcmPushNotificationToken) {
        this.fcmPushNotificationToken = fcmPushNotificationToken;
    }

    public String getBluetoothMacAddress() {
        return bluetoothMacAddress;
    }

    public void setBluetoothMacAddress(String bluetoothMacAddress) {
        this.bluetoothMacAddress = bluetoothMacAddress;
    }

    public boolean isPaired() {
        return paired;
    }

    public void setPaired(boolean paired) {
        this.paired = paired;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PairEntity)) return false;
        PairEntity entity = (PairEntity) o;
        return Objects.equals(getEmail(), entity.getEmail()) &&
                Objects.equals(getDeviceId(), entity.getDeviceId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail(), getDeviceId());
    }
}
