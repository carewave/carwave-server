package com.volme.pairing.config.logging;

import java.io.IOException;
import java.util.logging.*;

public final class PairingServiceLogger {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void setup() {
        LogManager.getLogManager().reset();
        LOGGER.setLevel(Level.ALL);

        try {
            FileHandler fileHandler = new FileHandler("pairing-service.log");
            fileHandler.setFormatter(new SimpleFormatter());
            fileHandler.setLevel(Level.FINE);
            LOGGER.addHandler(fileHandler);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "pairing-service file logger doesn\'t work!", e);
        }

    }
}
