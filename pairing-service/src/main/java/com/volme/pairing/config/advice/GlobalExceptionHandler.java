package com.volme.pairing.config.advice;

import com.volme.pairing.exception.ObjectNotFoundError;
import com.volme.pairing.exception.PairDaoException;
import com.volme.pairing.exception.PairingServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = Logger.getLogger(GlobalExceptionHandler.class.getName());
    private static final String DEFAULT_EXCEPTION_MESSAGE = "An exception has occurred in controller: ";
    private static final String LOCALE_MESSAGE = "The locale is: ";

    /**
     * VolmeServerException exception handler.
     *
     * @param e             exception.
     * @param handlerMethod information about method.
     * @param locale        localization.
     * @return ResponseEntity with CareWaveServerException payload.
     */
    @ExceptionHandler(value = PairingServiceException.class)
    public ResponseEntity<PairingServiceException> onPairingServiceException(PairingServiceException e,
                                                                             HandlerMethod handlerMethod,
                                                                             Locale locale) {
        Class controllerClass = handlerMethod.getMethod().getDeclaringClass();
        String message = String.format("%s%s . %s%s", DEFAULT_EXCEPTION_MESSAGE, controllerClass.toString(),
                LOCALE_MESSAGE, locale.toString());
        LOGGER.log(Level.SEVERE, message, e);

        return new ResponseEntity<>(new PairingServiceException(String.format("%s%s", message,
                e)), HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(value = PairDaoException.class)
    public ResponseEntity<PairDaoException> onPairDtoException(PairDaoException e,
                                                               HandlerMethod handlerMethod, Locale locale) {
        Class controllerClass = handlerMethod.getMethod().getDeclaringClass();
        String message = String.format("%s%s . %s%s", DEFAULT_EXCEPTION_MESSAGE, controllerClass.toString(),
                LOCALE_MESSAGE, locale.toString());
        LOGGER.log(Level.SEVERE, message, e);

        return new ResponseEntity<>(new PairDaoException(String.format("%s%s", message,
                e)), HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(ObjectNotFoundError.class)
    public ResponseEntity<ObjectNotFoundError> onObjectNotFountError(ObjectNotFoundError e,
                                                                     HandlerMethod handlerMethod, Locale locale) {
        Class controllerClass = handlerMethod.getMethod().getDeclaringClass();
        String message = String.format("%s%s . %s%s", DEFAULT_EXCEPTION_MESSAGE, controllerClass.toString(),
                LOCALE_MESSAGE, locale.toString());
        LOGGER.log(Level.SEVERE, message, e);

        return new ResponseEntity<>(new ObjectNotFoundError(String.format("%s %s", message, e)),
                HttpStatus.NOT_FOUND);
    }

    /**
     * General exception handler.
     *
     * @param e             exception.
     * @param handlerMethod handler method.
     * @param locale        locale.
     * @return ResponseEntity with Object payload.
     */

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> onException(Exception e, HandlerMethod handlerMethod, Locale locale) {
        Class controllerClass = handlerMethod.getMethod().getDeclaringClass();
        String message = String.format("%s%s . %s%s", DEFAULT_EXCEPTION_MESSAGE, controllerClass.toString(),
                LOCALE_MESSAGE, locale.toString());
        LOGGER.log(Level.SEVERE, message, e);

        return new ResponseEntity<>(String.format("An error has just occurred: \n %s \n%s", message, e),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}