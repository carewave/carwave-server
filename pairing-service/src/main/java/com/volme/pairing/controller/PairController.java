package com.volme.pairing.controller;

import com.volme.pairing.dto.rest.request.PairRequest;
import com.volme.pairing.dto.rest.response.PairResponse;
import com.volme.pairing.service.PairingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/v1")
public class PairController {

    private static Logger logger = Logger.getLogger(PairController.class.getName());

    private PairingService pairingService;

    @Autowired
    public PairController(PairingService pairingService) {
        this.pairingService = pairingService;
    }

    @PostMapping(value = "/pair", produces = APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PairResponse> pair(@RequestBody @Valid PairRequest pairRequest) {
        PairResponse response = pairingService.pair(pairRequest);
        if (response.wasAlreadyPaired()) {
            return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping(path = "/unpair", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<PairResponse> unpair(@RequestBody @Valid PairRequest pairRequest) {
        PairResponse response = pairingService.unpair(pairRequest);
        if (response.wasAlreadyPaired()) {
            return new ResponseEntity<>(response, HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PairResponse>> getAllPairs() {
        return new ResponseEntity<>(pairingService.getAllPairs(), HttpStatus.OK);
    }

    @GetMapping(value = "/{deviceId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<PairResponse>> getAllPairsByDeviceId(@PathVariable String deviceId) {
        return new ResponseEntity<>(pairingService.getPairedPairsByDeviceId(deviceId), HttpStatus.OK);
    }
}

