package com.volme.pairing.repository;

import com.volme.pairing.model.PairEntity;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface PairEntityRepository extends CrudRepository<PairEntity, Long> {
    @Override
    <S extends PairEntity> S save(S s);

    Optional<PairEntity> findByEmailAndDeviceId(String email, String deviceId);

   Collection<PairEntity> findAllByDeviceId(String deviceId);

    @Modifying(clearAutomatically = true)
    @Query("UPDATE PairEntity e SET e.paired = :paired WHERE e.email = :email AND e.deviceId = :deviceId")
    int update(@Param("paired") boolean paired, @Param("email") String email, @Param("deviceId") String deviceId);

    @Override
    Iterable<PairEntity> findAll();
}