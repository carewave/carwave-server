package com.volme.pairing.dao;

import com.volme.pairing.model.PairEntity;

import java.util.Collection;
import java.util.List;

public interface PairDao {

    PairEntity save(PairEntity entity);

    PairEntity findByEmailAndDeviceId(String email, String deviceId);

    List<PairEntity> findAllByDeviceId(String deviceId);

    int update(boolean paired, String email, String deviceId);

    Collection<PairEntity> findAllPairs();
}
