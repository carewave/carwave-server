package com.volme.pairing.dao;

import com.volme.pairing.exception.ObjectNotFoundError;
import com.volme.pairing.exception.PairDaoException;
import com.volme.pairing.model.PairEntity;
import com.volme.pairing.repository.PairEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class PairDaoImpl implements PairDao {
    private static Logger logger = Logger.getLogger(PairDaoImpl.class.getName());

    private PairEntityRepository pairEntityRepository;

    @Autowired
    public PairDaoImpl(PairEntityRepository pairEntityRepository) {
        this.pairEntityRepository = pairEntityRepository;
    }

    @Override
    public PairEntity save(PairEntity entity) {
        logger.info(String.format("The pair entity of email '%s' and device is '%s' has been saved successfully."
                , entity.getEmail(), entity.getDeviceId()));
        return pairEntityRepository.save(entity);
    }

    @Override
    public PairEntity findByEmailAndDeviceId(String email, String deviceId) {
        if (pairEntityRepository.findByEmailAndDeviceId(email, deviceId).isPresent()) {
            logger.info(String.format("The entity has been found successfully by email '%s' and device id '%s'.", email,
                    deviceId));
            return pairEntityRepository.findByEmailAndDeviceId(email, deviceId).get();
        } else {
            logger.info(String.format("Entity with email '%s' and device id '%s' wasn't found!", email, deviceId));
            throw new ObjectNotFoundError(String.format("There is no pair with email '%s' and device id '%s'.", email,
                    deviceId));
        }
    }

    @Override
    public List<PairEntity> findAllByDeviceId(String deviceId) {
        return new ArrayList<>(pairEntityRepository.findAllByDeviceId(deviceId));
    }

    @Transactional
    @Override
    public int update(boolean paired, String email, String deviceId) {
        int result = pairEntityRepository.update(paired, email, deviceId);
        if (result == 0) {
            logger.severe("The update operation failed: 0 rows were affected!");
            throw new PairDaoException("'update' operation error - 0 rows were affected!");
        } else {
            return result;
        }
    }

    @Override
    public List<PairEntity> findAllPairs() {
        return StreamSupport.stream(pairEntityRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }
}
