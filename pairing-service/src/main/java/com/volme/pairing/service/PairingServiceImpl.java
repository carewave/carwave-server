package com.volme.pairing.service;

import com.volme.pairing.dao.PairDao;
import com.volme.pairing.dto.rest.request.PairRequest;
import com.volme.pairing.dto.rest.response.PairResponse;
import com.volme.pairing.exception.ObjectNotFoundError;
import com.volme.pairing.model.PairEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class PairingServiceImpl implements PairingService {
    private static Logger logger = Logger.getLogger(PairingServiceImpl.class.getName());
    private PairDao pairDao;

    public PairingServiceImpl(PairDao pairDao) {
        this.pairDao = pairDao;
    }

    @Override
    public <S extends PairRequest> PairResponse pair(S pairRequest) {
        String email = pairRequest.getEmail();
        String deviceId = pairRequest.getDeviceId();
        PairEntity pairEntity;
        try {
            pairEntity = pairDao.findByEmailAndDeviceId(email, deviceId);
        } catch (ObjectNotFoundError e) {
            logger.info("Saving new entity...");
            return toPairResponse(pairDao.save(toPairEntity(pairRequest)));
        }
        PairEntity updatedEntity;
        if (!pairEntity.isPaired()) {
            logger.info("Pairing an existing pair...");
            pairDao.update(true, email, deviceId);
            updatedEntity = pairDao.findByEmailAndDeviceId(email, deviceId);
        } else {
            logger.info("The pair has already been paired. Proceeding without any changes...");
            return toPairResponse(pairEntity, true);
        }

        logger.info(String.format("Email '%s' and device with id '%s' where paired successfully!"
                , pairRequest.getEmail(), pairRequest.getDeviceId()));
        return toPairResponse(updatedEntity);
    }

    @Override
    public <S extends PairRequest> PairResponse unpair(S unpairRequest) {
        String email = unpairRequest.getEmail();
        String deviceId = unpairRequest.getDeviceId();
        PairEntity pairEntity = pairDao.findByEmailAndDeviceId(email, deviceId);

        if (pairEntity.isPaired()) {
            logger.info("Unpairing an existing pair...");
            pairDao.update(false, email, deviceId);
            logger.info("The pair has been successfully unpaired!");
        } else {
            logger.info("The pair has already been unpaired! Proceeding without any changes.");
            return toPairResponse(pairEntity, true);
        }
        logger.info(String.format("Email '%s' and device with id '%s' where unpaired successfully!"
                , unpairRequest.getEmail(), unpairRequest.getDeviceId()));
        return toPairResponse(pairDao.findByEmailAndDeviceId(email, deviceId));
    }

    @Override
    public PairResponse getPairByEmailAndDeviceId(String email, String deviceId) {
        return toPairResponse(pairDao.findByEmailAndDeviceId(email, deviceId));
    }

    @Override
    public List<PairResponse> getPairedPairsByDeviceId(String deviceId) {
        return pairDao.findAllByDeviceId(deviceId)
                .stream()
                .filter(PairEntity::isPaired)
                .map(this::toPairResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<PairResponse> getAllPairs() {
        return pairDao.findAllPairs()
                .stream()
                .map(this::toPairResponse)
                .collect(Collectors.toList());
    }

    private <S extends PairRequest> PairEntity toPairEntity(S request) {
        return new PairEntity(request.getEmail(), request.getDeviceId(), request.getFcmPushNotificationToken(),
                request.getBluetoothMacAddress(), true);
    }

    private PairResponse toPairResponse(PairEntity pairEntity) {
        return new PairResponse(
                pairEntity.getEmail(), pairEntity.getDeviceId(), pairEntity.getFcmPushNotificationToken(),
                pairEntity.getBluetoothMacAddress(), pairEntity.isPaired()
        );
    }

    private PairResponse toPairResponse(PairEntity pairEntity, boolean wasPaired) {
        return new PairResponse(
                pairEntity.getEmail(), pairEntity.getDeviceId(), pairEntity.getFcmPushNotificationToken(),
                pairEntity.getBluetoothMacAddress(), pairEntity.isPaired(),
                wasPaired
        );
    }
}
