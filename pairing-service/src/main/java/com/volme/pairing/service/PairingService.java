package com.volme.pairing.service;

import com.volme.pairing.dto.rest.request.PairRequest;
import com.volme.pairing.dto.rest.response.PairResponse;

import java.util.List;

public interface PairingService {

    <S extends PairRequest> PairResponse pair(S pairRequest);

    <S extends PairRequest> PairResponse unpair(S unpairRequest);

    PairResponse getPairByEmailAndDeviceId(String email, String deviceId);

    List<PairResponse> getPairedPairsByDeviceId(String deviceId);

    List<PairResponse> getAllPairs();
}
