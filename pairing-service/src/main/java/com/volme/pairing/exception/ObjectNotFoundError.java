package com.volme.pairing.exception;

public class ObjectNotFoundError extends RuntimeException {

    public ObjectNotFoundError(String message) {
        super(message);
    }
}
