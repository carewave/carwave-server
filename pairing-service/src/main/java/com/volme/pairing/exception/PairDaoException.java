package com.volme.pairing.exception;

public class PairDaoException extends RuntimeException {
    public PairDaoException(String message) {
        super(message);
    }

    public PairDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
