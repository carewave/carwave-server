package com.volme.pairing.exception;

public class PairingServiceException extends RuntimeException {

    public PairingServiceException(String message) {
        super(message);
    }

    public PairingServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
