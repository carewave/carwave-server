package com.volme.pairing.dto.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PairRequest {
    @Email(message = "'email' field should correspond to email standard!")
    private String email;

    @NotNull(message = "'deviceId' cannot be null!")
    private String deviceId;

    @JsonProperty("fcm_push_notification_token")
    @NotNull(message = "'FCM token' cannot be null!")
    private String fcmPushNotificationToken;

    @JsonProperty("bluetooth_mac_address")
    @NotNull(message = "'BT MAC address' cannot be null!")
    private String bluetoothMacAddress;

}
