package com.volme.notificationservice.service;

import com.volme.notificationservice.firebase.FCMService;
import com.volme.notificationservice.model.PushNotificationRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PushNotificationServiceTest {

    @Mock
    private FCMService fcmService;

    @InjectMocks
    private PushNotificationServiceImpl subject;

    @Test
    public void sendPushNotificationToTopic_shouldPrepareCorrectMessageAndTriggerFCMServiceMethod()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        doNothing().when(fcmService).sendMessageWithoutData(request);

        //when
        subject.sendPushNotificationToTopic(request);

        //then
        verify(fcmService, times(1)).sendMessageWithoutData(request);
    }

    @Test(expected = ExecutionException.class)
    public void sendPushNotificationToTopic_shouldThrowExecutionExceptionIfFCMServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        doThrow(ExecutionException.class).when(fcmService).sendMessageWithoutData(request);

        //when
        subject.sendPushNotificationToTopic(request);

        //then ExecutionException thrown
    }

    @Test(expected = InterruptedException.class)
    public void sendPushNotificationToTopic_shouldThrowInterruptedExceptionIfFCMServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        doThrow(InterruptedException.class).when(fcmService).sendMessageWithoutData(request);

        //when
        subject.sendPushNotificationToTopic(request);

        //then InterruptedException thrown
    }

    @Test
    public void sendPushNotificationToToken_shouldPrepareCorrectMessageAndTriggerFCMServiceMethod()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        doNothing().when(fcmService).sendMessageToToken(request);

        //when
        subject.sendPushNotificationToToken(request);

        //then
        verify(fcmService, times(1)).sendMessageToToken(request);
    }

    @Test(expected = ExecutionException.class)
    public void sendPushNotificationToToken_shouldThrowExecutionExceptionIfFCMServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        doThrow(ExecutionException.class).when(fcmService).sendMessageToToken(request);

        //when
        subject.sendPushNotificationToToken(request);

        //then ExecutionException thrown
    }

    @Test(expected = InterruptedException.class)
    public void sendPushNotificationToToken_shouldThrowInterruptedExceptionIfFCMServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        doThrow(InterruptedException.class).when(fcmService).sendMessageToToken(request);

        //when
        subject.sendPushNotificationToToken(request);

        //then InterruptedException thrown
    }

}