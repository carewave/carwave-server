package com.volme.notificationservice.controller;

import com.volme.notificationservice.model.PushNotificationRequest;
import com.volme.notificationservice.model.PushNotificationResponse;
import com.volme.notificationservice.service.PushNotificationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.concurrent.ExecutionException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.Silent.class)
public class PushNotificationControllerTest {

    @Mock
    private PushNotificationService pushNotificationService;

    @InjectMocks
    private PushNotificationController subject;


    @Test
    public void sendTopicNotification_shouldTriggerServiceMethodWithCorrectParametersAndRespondWith200Status()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String notificationMessage = "Notification has been sent.";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        PushNotificationResponse response = new PushNotificationResponse(200, notificationMessage);
        ResponseEntity<PushNotificationResponse> expected = new ResponseEntity<>(response, HttpStatus.OK);
        doNothing().when(pushNotificationService).sendPushNotificationToTopic(request);

        //when
        ResponseEntity<PushNotificationResponse> actual = subject.sendTopicNotification(request);

        //then
        verify(pushNotificationService, times(1)).sendPushNotificationToTopic(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test(expected = ExecutionException.class)
    public void sendTopicNotification_shouldThrowExecutionExceptionWhenServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        doThrow(ExecutionException.class).when(pushNotificationService).sendPushNotificationToTopic(request);

        //when
        subject.sendTopicNotification(request);

        //then ExecutionException is thrown
    }

    @Test(expected = InterruptedException.class)
    public void sendTopicNotification_shouldThrowInterruptedExceptionWhenServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null);
        doThrow(InterruptedException.class).when(pushNotificationService).sendPushNotificationToTopic(request);

        //when
        subject.sendTopicNotification(request);

        //then InterruptedException is thrown
    }


    @Test
    public void sendTokenNotification_shouldTriggerServiceMethodWithCorrectParametersAndRespondWith200Status()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        String notificationMessage = "Notification has been sent.";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        PushNotificationResponse response = new PushNotificationResponse(200, notificationMessage);
        ResponseEntity<PushNotificationResponse> expected = new ResponseEntity<>(response, HttpStatus.OK);
        doNothing().when(pushNotificationService).sendPushNotificationToToken(request);

        //when
        ResponseEntity<PushNotificationResponse> actual = subject.sendTopicNotification(request);

        //then
        verify(pushNotificationService, times(1)).sendPushNotificationToTopic(request);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test(expected = ExecutionException.class)
    public void sendTokenNotification_shouldThrowExecutionExceptionWhenServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        doThrow(ExecutionException.class).when(pushNotificationService).sendPushNotificationToToken(request);

        //when
        subject.sendTokenNotification(request);

        //then ExecutionException is thrown
    }

    @Test(expected = InterruptedException.class)
    public void sendTokencNotification_shouldThrowInterruptedExceptionWhenServiceThrows()
            throws ExecutionException, InterruptedException {
        //given
        String title = "Dynamo";
        String message = "Kyiv";
        String topic = "football";
        String token = "token";
        PushNotificationRequest request = new PushNotificationRequest(title, message, topic, null, token);
        doThrow(InterruptedException.class).when(pushNotificationService).sendPushNotificationToToken(request);

        //when
        subject.sendTokenNotification(request);

        //then InterruptedException is thrown
    }

}