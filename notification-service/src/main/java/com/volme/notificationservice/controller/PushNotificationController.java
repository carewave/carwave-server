package com.volme.notificationservice.controller;

import com.volme.notificationservice.model.PushNotificationRequest;
import com.volme.notificationservice.model.PushNotificationResponse;
import com.volme.notificationservice.service.PushNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
public class PushNotificationController {
    private PushNotificationService pushNotificationService;

    @Autowired
    public PushNotificationController(PushNotificationService pushNotificationService) {
        this.pushNotificationService = pushNotificationService;
    }

    @PostMapping("/topic")
    public ResponseEntity<PushNotificationResponse> sendTopicNotification(@RequestBody PushNotificationRequest request)
            throws ExecutionException, InterruptedException {
        pushNotificationService.sendPushNotificationToTopic(request);
        return new ResponseEntity<>(
                new PushNotificationResponse(
                        HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

    @PostMapping("/token")
    public ResponseEntity<PushNotificationResponse> sendTokenNotification(@RequestBody PushNotificationRequest request)
            throws ExecutionException, InterruptedException {
        pushNotificationService.sendPushNotificationToToken(request);
        return new ResponseEntity<>(
                new PushNotificationResponse(
                        HttpStatus.OK.value(), "Notification has been sent."), HttpStatus.OK);
    }

}
