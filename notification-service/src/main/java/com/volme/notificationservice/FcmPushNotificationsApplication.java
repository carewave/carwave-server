package com.volme.notificationservice;

import com.volme.notificationservice.config.logging.NotificationServiceLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FcmPushNotificationsApplication {

    public static void main(String[] args) {
        SpringApplication.run(FcmPushNotificationsApplication.class, args);
        NotificationServiceLogger.setup();
    }

}
