package com.volme.notificationservice.service;

import com.volme.notificationservice.firebase.FCMService;
import com.volme.notificationservice.model.PushNotificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class PushNotificationServiceImpl implements PushNotificationService {

    private Logger logger = LoggerFactory.getLogger(PushNotificationServiceImpl.class);
    private FCMService fcmService;

    @Autowired
    public PushNotificationServiceImpl(FCMService fcmService) {
        this.fcmService = fcmService;
    }

    public void sendPushNotificationToTopic(PushNotificationRequest request) throws ExecutionException,
            InterruptedException {
        fcmService.sendMessageWithoutData(request);
    }

    public void sendPushNotificationToToken(PushNotificationRequest request) throws ExecutionException,
            InterruptedException {
        fcmService.sendMessageToToken(request);

    }

}
