package com.volme.notificationservice.service;

import com.volme.notificationservice.model.PushNotificationRequest;

import java.util.concurrent.ExecutionException;

public interface PushNotificationService {

    void sendPushNotificationToTopic(PushNotificationRequest request) throws ExecutionException, InterruptedException;

    void sendPushNotificationToToken(PushNotificationRequest request) throws ExecutionException, InterruptedException;
}
