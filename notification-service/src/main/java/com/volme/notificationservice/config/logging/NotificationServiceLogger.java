package com.volme.notificationservice.config.logging;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public final class NotificationServiceLogger {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void setup() {
        LogManager.getLogManager().reset();
        LOGGER.setLevel(Level.ALL);

        try {
            FileHandler fileHandler = new FileHandler("notification-service.log");
            fileHandler.setFormatter(new SimpleFormatter());
            fileHandler.setLevel(Level.FINE);
            LOGGER.addHandler(fileHandler);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "notification-service file logger doesn\'t work!", e);
        }

    }
}
