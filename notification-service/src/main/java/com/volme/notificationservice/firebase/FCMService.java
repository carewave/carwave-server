package com.volme.notificationservice.firebase;

import com.volme.notificationservice.model.PushNotificationRequest;

import java.util.concurrent.ExecutionException;

public interface FCMService {

    void sendMessageWithoutData(PushNotificationRequest request) throws InterruptedException, ExecutionException;

    void sendMessageToToken(PushNotificationRequest request) throws InterruptedException, ExecutionException;
}
