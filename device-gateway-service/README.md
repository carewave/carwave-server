                                                **DEVICE-GATEWAY POINT**
                                                
The module performs as a gateway for the device-related HTTP calls. 

It mostly intended for the authorization calls: to validate JWT tokens. It also redirect request to obtain tokens to 
the device authorization service. Once token's signature recognized as a valid one - the authentication context is 
filed by a corresponding Authentication object with claims, that are specified in a token itself. Token parsing and 
filling the Authentication object happens automatically by the 'Spring Security OAuth2' library.