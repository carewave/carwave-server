package com.volme.gateway.config.zuul;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.ROUTE_TYPE;
import static org.springframework.cloud.netflix.zuul.filters.support.FilterConstants.SIMPLE_HOST_ROUTING_FILTER_ORDER;

@Component
public class HeadersFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return ROUTE_TYPE;
    }

    @Override
    public int filterOrder() {
        return SIMPLE_HOST_ROUTING_FILTER_ORDER - 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        //TODO: Tech dept: need to provide a correct headers management. Currently, the requests work only because we
        // don't need to use different set of headers for the downstream services (the ones, behind Zuul) but this is
        // only a matter of time.
        RequestContext ctx = RequestContext.getCurrentContext();
        if (SecurityContextHolder.getContext() != null) {
            ctx.remove("zuulRequestHeaders", "content-type");
            ctx.addZuulRequestHeader("Content-type", "application/json");
        }
        return null;
    }
}
