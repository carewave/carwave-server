package com.volme.gateway.config.spring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.jwt.crypto.sign.Signer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

@Configuration
@EnableResourceServer
@PropertySource(value = "classpath:bootstrap.yml")
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    private static final String ENCRYPTION_ALGORITHM = "HMACSHA512";

    @Value("${secret.key}")
    private String secretJwtKey;

    @Override
    public void configure(ResourceServerSecurityConfigurer config) {
        config.resourceId("api-gateway").tokenStore(tokenStore());
        config.stateless(true);
        config.tokenServices(tokenServices());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/api-docs/**").permitAll()
                .anyRequest().authenticated();
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        SecretKey secretKey = new SecretKeySpec(secretJwtKey.getBytes(), ENCRYPTION_ALGORITHM);
        Signer signer = new MacSigner(ENCRYPTION_ALGORITHM, secretKey);
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setVerifier(new MacSigner(ENCRYPTION_ALGORITHM, secretKey));
        converter.setSigner(signer);
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        return defaultTokenServices;
    }

}