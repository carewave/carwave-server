package com.volme.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class DeviceGatewayApplication {
	//TODO: Add proper logging for token validation errors: currently, if a token is invalid or expired it's impossible
	// to see it in the logs. The only source of info about what is actually going on - are server JSON responses.

	public static void main(String[] args) {
		SpringApplication.run(DeviceGatewayApplication.class, args);
	}
}
