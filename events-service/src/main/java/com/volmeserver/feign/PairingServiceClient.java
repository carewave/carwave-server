package com.volmeserver.feign;

import com.volmeserver.dto.rest.request.Pair;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient("pairing-service")
public interface PairingServiceClient {

    @GetMapping(value = "/pairing/v1/{deviceId}")
    List<Pair> getPairsByDeviceId(@PathVariable("deviceId") String deviceId);
}
