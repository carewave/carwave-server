package com.volmeserver.feign;

import com.volmeserver.dto.rest.request.PushNotificationRequest;
import com.volmeserver.dto.rest.response.PushNotificationResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("notification-service")
public interface NotificationServiceClient {

    @GetMapping(value = "/notification/token")
    PushNotificationResponse sendNotificationToDevice(PushNotificationRequest request);
}
