package com.volmeserver.configuration.logging;

import java.io.IOException;
import java.util.logging.*;

public final class VolmeServerLogger {

    private static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void setup() {
        LogManager.getLogManager().reset();
        LOGGER.setLevel(Level.ALL);

        try {
            FileHandler fileHandler = new FileHandler("volme-server.log");
            fileHandler.setFormatter(new SimpleFormatter());
            fileHandler.setLevel(Level.FINE);
            LOGGER.addHandler(fileHandler);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "File logger doesn\'t work!", e);
        }

    }
}
