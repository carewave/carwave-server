package com.volmeserver.dao;

import com.volmeserver.model.VolumeChangeEventEntity;
import com.volmeserver.exception.VolmeServerException;
import com.volmeserver.repository.VolumeChangeEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VolumeChangeEventDaoImpl implements VolumeChangeEventDao {
    private VolumeChangeEventRepository repository;

    @Autowired
    public VolumeChangeEventDaoImpl(VolumeChangeEventRepository repository) {
        this.repository = repository;
    }

    @Override
    public VolumeChangeEventEntity save(VolumeChangeEventEntity event) {
        return repository.save(event);
    }

    @Override
    public VolumeChangeEventEntity findById(Long id) {
        if (repository.findById(id).isPresent()) {
            return repository.findById(id).get();
        } else throw new VolmeServerException(String.format("There is no entity with '%s' id!", id));
    }

    @Override
    public List<VolumeChangeEventEntity> findAll() {
        return (List<VolumeChangeEventEntity>) repository.findAll();
    }

    @Override
    public List<VolumeChangeEventEntity> findAllById(List<Long> id) {
        return (List<VolumeChangeEventEntity>) repository.findAllById(id);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public boolean doesExist(Long id) {
        return repository.existsById(id);
    }
}
