package com.volmeserver.dao;

import com.volmeserver.model.VolumeChangeEventEntity;

import java.util.List;

public interface VolumeChangeEventDao {
    VolumeChangeEventEntity save(VolumeChangeEventEntity event);

    VolumeChangeEventEntity findById(Long id);

    List<VolumeChangeEventEntity> findAll();

    List<VolumeChangeEventEntity> findAllById(List<Long> id);

    void deleteAll();

    void deleteById(Long id);

    boolean doesExist(Long id);
}
