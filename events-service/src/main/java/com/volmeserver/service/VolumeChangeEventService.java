package com.volmeserver.service;

import com.volmeserver.dto.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.rest.request.BaseEvent;
import com.volmeserver.dto.rest.response.VehicleVolumeChangeEventResponse;

import java.util.List;

public interface VolumeChangeEventService {

    <S extends BaseEvent> void processEvent(S event);

    VolumeChangeEventEntityResponse getEvent(Long id);

    List<VolumeChangeEventEntityResponse> getEvents(List<Long> ids);

    void deleteEvent(Long id);

    List<VolumeChangeEventEntityResponse> getAllEvents();

    void deleteAllEvents();

    VehicleVolumeChangeEventResponse doesExist(Long id);
}
