package com.volmeserver.service;

import com.volmeserver.dto.rest.request.Pair;
import com.volmeserver.dto.rest.request.PushNotificationRequest;
import com.volmeserver.dto.rest.response.PushNotificationResponse;
import com.volmeserver.exception.NotificationException;
import com.volmeserver.feign.NotificationServiceClient;
import com.volmeserver.utils.NotificationRequestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@Service
public class NotificationServiceImpl implements NotificationService {
    private static Logger logger = Logger.getLogger(NotificationServiceImpl.class.getSimpleName());
    private static final String NOTIFICATION_SERVICE_HTTP_ERROR_MESSAGE = "A problem occurred during notification sending: status code - %s, message - %s";
    private static final String UNSUPPORTED_EVENT_TYPE_ERROR_MESSAGE = "A problem occurred during notification sending: unsupported event type: %s";
    private NotificationServiceClient notificationServiceClient;

    @Autowired
    public NotificationServiceImpl(NotificationServiceClient notificationServiceClient) {
        this.notificationServiceClient = notificationServiceClient;
    }

    @Override
    public void notifyUsers(String eventType, List<Pair> pairs) {
        pairs.forEach(p -> {
            logger.info(String.format("Sending a notification to %s user", p.getEmail()));
            Optional<PushNotificationRequest> request = NotificationRequestFactory.getNotificationRequest(
                    eventType, p.getFcmPushNotificationToken());

            if (request.isPresent()) {
                PushNotificationResponse responseFromNotificationService = notificationServiceClient
                        .sendNotificationToDevice(request.get());

                if (HttpStatus.OK.value() == responseFromNotificationService.getStatus()) {
                    logger.info("The notification has been successfully sent!");
                } else throw new NotificationException(
                        String.format(NOTIFICATION_SERVICE_HTTP_ERROR_MESSAGE,
                                responseFromNotificationService.getStatus(),
                                responseFromNotificationService.getMessage()));
            } else
                throw new NotificationException(String.format(
                        UNSUPPORTED_EVENT_TYPE_ERROR_MESSAGE, eventType));
        });
    }
}
