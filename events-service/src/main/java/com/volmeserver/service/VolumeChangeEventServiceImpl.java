package com.volmeserver.service;

import com.volmeserver.dao.VolumeChangeEventDao;
import com.volmeserver.dto.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.rest.request.BaseEvent;
import com.volmeserver.dto.rest.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.feign.PairingServiceClient;
import com.volmeserver.model.VolumeChangeEventEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VolumeChangeEventServiceImpl implements VolumeChangeEventService {
    private Clock clock;
    private VolumeChangeEventDao volumeChangeEventDao;
    private PairingServiceClient pairingServiceClient;
    private NotificationService notificationService;

    @Autowired
    public VolumeChangeEventServiceImpl(VolumeChangeEventDao volumeChangeEventDao,
                                        PairingServiceClient pairingServiceClient,
                                        NotificationService notificationService,
                                        Clock clock) {
        this.volumeChangeEventDao = volumeChangeEventDao;
        this.clock = clock;
        this.pairingServiceClient = pairingServiceClient;
        this.notificationService = notificationService;
    }

    @Override
    public <S extends BaseEvent> void processEvent(S event) {
        notificationService.notifyUsers(event.getEventType(), pairingServiceClient.getPairsByDeviceId(String.valueOf(
                event.getDeviceId())));

        VolumeChangeEventEntity entity = new VolumeChangeEventEntity(event.getEventType(), event.getDeviceId(),
                event.getLocation(), event.getDate());
        volumeChangeEventDao.save(entity);
    }

    @Override
    public VolumeChangeEventEntityResponse getEvent(Long id) {
        return convertToBaseType(volumeChangeEventDao.findById(id));
    }

    @Override
    public List<VolumeChangeEventEntityResponse> getEvents(List<Long> ids) {
        return volumeChangeEventDao.findAllById(ids).stream().map(this::convertToBaseType)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteEvent(Long id) {
        volumeChangeEventDao.deleteById(id);
    }

    @Override
    public void deleteAllEvents() {
        volumeChangeEventDao.deleteAll();
    }

    @Override
    public VehicleVolumeChangeEventResponse doesExist(Long id) {
        if (volumeChangeEventDao.doesExist(id)) {
            return new VehicleVolumeChangeEventResponse("The event exists in the database",
                    LocalDateTime.now(clock));
        } else return new VehicleVolumeChangeEventResponse("The event doesn't exist in the database",
                LocalDateTime.now(clock));
    }

    @Override
    public List<VolumeChangeEventEntityResponse> getAllEvents() {
        return volumeChangeEventDao.findAll().stream().map(this::convertToBaseType).collect(Collectors.toList());
    }

    private VolumeChangeEventEntityResponse convertToBaseType(VolumeChangeEventEntity eventEntity) {
        return new VolumeChangeEventEntityResponse(eventEntity.getId(), eventEntity.getEventType(),
                eventEntity.getDeviceId(), eventEntity.getLocation(), eventEntity.getDate());

    }
}
