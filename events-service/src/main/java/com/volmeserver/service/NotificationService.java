package com.volmeserver.service;

import com.volmeserver.dto.rest.request.Pair;

import java.util.List;

public interface NotificationService {

    void notifyUsers(String eventType, List<Pair> pairs);
}
