package com.volmeserver.dto.rest.request;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Getter
@Setter
@EqualsAndHashCode
public abstract class BaseEvent {
    @NotNull(message = "Event type cannot be null!")
    private String eventType;
    @NotNull(message = "Device id cannot be null!")
    private Long deviceId;
    @NotNull(message = "Date cannot be null!")
    private String location;
    private LocalDateTime date;

    protected BaseEvent() {

    }

    protected BaseEvent(String eventType, Long deviceId, String location, String date) {
        this.eventType = eventType;
        this.deviceId = deviceId;
        this.location = location;
        this.date = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(date)), ZoneId.systemDefault());
    }

    @Override
    public String toString() {

        return this.getClass().getSimpleName() + "{" +
                "event_type='" + eventType + '\'' +
                ", device_id='" + deviceId +
                ", location='" + location + '\'' +
                ", date='" + date +
                '}';
    }
}