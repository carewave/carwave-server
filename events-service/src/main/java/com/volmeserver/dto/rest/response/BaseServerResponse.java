package com.volmeserver.dto.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class BaseServerResponse {
    @JsonProperty("status_description")
    private String statusDescription;

    @JsonProperty("date")
    private LocalDateTime date;

     BaseServerResponse() {

    }

     BaseServerResponse(String statusDescription, LocalDateTime date) {
        this.statusDescription = statusDescription;
        this.date = date;
    }

     BaseServerResponse(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "{" +
                "status_description='" + statusDescription + '\'' +
                ", date=" + date +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseServerResponse)) return false;
        BaseServerResponse that = (BaseServerResponse) o;
        return Objects.equals(statusDescription, that.statusDescription) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(statusDescription, date);
    }
}
