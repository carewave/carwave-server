package com.volmeserver.dto.rest.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class VehicleVolumeChangeEvent extends BaseEvent {

    @JsonCreator
    public VehicleVolumeChangeEvent(
            @JsonProperty("event_type") String eventType,
            @JsonProperty("device_id") Long deviceId,
            @JsonProperty("location") String location,
            @JsonProperty("date") String date
    ) {
        super(eventType, deviceId, location, date);
    }

}