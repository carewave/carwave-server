package com.volmeserver.dto.rest.request;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class PushNotificationRequest {

    private String title;
    private String message;
    private String topic;


    private String condition;
    private String token;

    public PushNotificationRequest() {
    }

    public PushNotificationRequest(String title, String messageBody, String topicName, String condition) {
        this.title = title;
        this.message = messageBody;
        this.topic = topicName;
        this.condition = condition;
    }

    public PushNotificationRequest(String title, String messageBody, String topicName, String condition, String token) {
        this.title = title;
        this.message = messageBody;
        this.topic = topicName;
        this.condition = condition;
        this.token = token;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
