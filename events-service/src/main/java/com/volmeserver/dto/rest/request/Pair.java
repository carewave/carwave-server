package com.volmeserver.dto.rest.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Pair {
    private String email;

    @JsonProperty("device_id")
    private String deviceId;

    @JsonProperty("fcm_push_notification_token")
    private String fcmPushNotificationToken;

    @JsonProperty("bluetooth_mac_address")
    private String bluetoothMacAddress;

    private boolean paired;

    @JsonIgnore
    private boolean wasPaired;

    public Pair() {

    }

    public Pair(String email, String deviceId, String fcmPushNotificationToken, String bluetoothMacAddress,
                        boolean paired) {
        this.email = email;
        this.deviceId = deviceId;
        this.paired = paired;
        this.fcmPushNotificationToken = fcmPushNotificationToken;
        this.bluetoothMacAddress = bluetoothMacAddress;
    }

    public String getEmail() {
        return email;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getFcmPushNotificationToken() {
        return fcmPushNotificationToken;
    }

    public String getBluetoothMacAddress() {
        return bluetoothMacAddress;
    }

    public boolean isPaired() {
        return paired;
    }

    public boolean wasAlreadyPaired() {
        return wasPaired;
    }
}
