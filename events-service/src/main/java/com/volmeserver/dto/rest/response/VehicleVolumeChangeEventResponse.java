package com.volmeserver.dto.rest.response;

import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
public final class VehicleVolumeChangeEventResponse extends BaseServerResponse {

    public VehicleVolumeChangeEventResponse(String statusDescription, LocalDateTime date) {
        super(statusDescription, date);
    }

    public VehicleVolumeChangeEventResponse(String statusDescription) {
        super(statusDescription);
    }

    public VehicleVolumeChangeEventResponse() {
        super();
    }
}