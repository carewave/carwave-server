package com.volmeserver.dto.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@EqualsAndHashCode
public final class VolumeChangeEventEntityResponse {
    private Long id;
    @JsonProperty("event_type")
    private String eventType;
    @JsonProperty("device_id")
    private Long deviceId;
    private String location;
    private LocalDateTime date;

    public VolumeChangeEventEntityResponse(Long id, String eventType, Long deviceId, String location,
                                           LocalDateTime date) {
        this.id = id;
        this.eventType = eventType;
        this.deviceId = deviceId;
        this.location = location;
        this.date = date;
    }

    @Override
    public String toString() {
        return "VolumeChangeEventEntityResponse{" +
                "id=" + id +
                ", eventType='" + eventType + '\'' +
                ", deviceId=" + deviceId +
                ", location='" + location + '\'' +
                ", date=" + date +
                '}';
    }

}
