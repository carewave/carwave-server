package com.volmeserver.controller;

import com.volmeserver.dto.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.rest.request.VehicleVolumeChangeEvent;
import com.volmeserver.dto.rest.response.BaseServerResponse;
import com.volmeserver.dto.rest.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.service.VolumeChangeEventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/event/volume-change-event")
public final class EventsController {
    private static Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private Clock clock;
    private VolumeChangeEventService volumeChangeEventService;

    public EventsController(VolumeChangeEventService volumeChangeEventService, Clock clock) {
        this.volumeChangeEventService = volumeChangeEventService;
        this.clock = clock;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/", produces = APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public void receiveVolumeChangeEvent(@Valid
                                         @RequestBody VehicleVolumeChangeEvent event) {
        logger.info(String.format("\n The following event has been received: %s\n", event.toString()));
        volumeChangeEventService.processEvent(event);
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE,
            consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<VolumeChangeEventEntityResponse> getEvent(@PathVariable Long id) {
        return new ResponseEntity<>(volumeChangeEventService.getEvent(id), HttpStatus.OK);
    }

    @GetMapping(value = "/events", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VolumeChangeEventEntityResponse>> getEvents(@RequestParam List<Long> ids) {
        return new ResponseEntity<>(volumeChangeEventService.getEvents(ids), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleVolumeChangeEventResponse> deleteEventById(@PathVariable Long id) {
        volumeChangeEventService.deleteEvent(id);
        return new ResponseEntity<>(new VehicleVolumeChangeEventResponse(
                "An event has been deleted successfully!", LocalDateTime.now(clock)), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VolumeChangeEventEntityResponse>> getAllEvents() {
        return new ResponseEntity<>(volumeChangeEventService.getAllEvents(), HttpStatus.OK);
    }

    @DeleteMapping(value = "/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<VehicleVolumeChangeEventResponse> deleteAllEvents() {
        volumeChangeEventService.deleteAllEvents();
        return new ResponseEntity<>(new VehicleVolumeChangeEventResponse(
                "All events have been deleted successfully!", LocalDateTime.now(clock)), HttpStatus.OK);
    }

    @GetMapping(value = "/does-exist/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseServerResponse> doesExits(@PathVariable Long id) {
        return new ResponseEntity<>(volumeChangeEventService.doesExist(id), HttpStatus.OK);

    }
}
