package com.volmeserver.utils;

import com.volmeserver.dto.rest.request.PushNotificationRequest;

import java.util.Optional;

public class NotificationRequestFactory {
    private static final String PENETRATION_ALERT = "penetration";
    private static final String ALARM_TRIGGERING = "alarm";
    private static final String PENETRATION_ALERT_TITLE = "!VEHICLE PENETRATION ALERT!";
    private static final String PENETRATION_ALERT_MESSAGE = "Someone is inside your car - click here for a live stream!";
    private static final String ALARM_TRIGGERING_TITLE = "!ALARM TRIGGERING ALERT!";
    private static final String ALARM_TRIGGERING_MESSAGE = "You'r alarm has just been activated - click here for a live stream!";
    private static final String DEFAULT_TOPIC = "default_topic";
    private static final String DEFAULT_CONDITION = "default_condition";

    public static Optional<PushNotificationRequest> getNotificationRequest(String requestType, String token) {
        if (requestType.equals(PENETRATION_ALERT)) {
            return Optional.of(new PushNotificationRequest(
                    PENETRATION_ALERT_TITLE, PENETRATION_ALERT_MESSAGE, DEFAULT_TOPIC,
                    DEFAULT_CONDITION, token));

        } else if (requestType.equals(ALARM_TRIGGERING)) {
            return Optional.of(new PushNotificationRequest(
                    ALARM_TRIGGERING_TITLE, ALARM_TRIGGERING_MESSAGE, DEFAULT_TOPIC,
                    DEFAULT_CONDITION, token));
        }
        return Optional.empty();
    }

}
