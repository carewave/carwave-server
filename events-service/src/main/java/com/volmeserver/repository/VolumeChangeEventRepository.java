package com.volmeserver.repository;

import com.volmeserver.model.VolumeChangeEventEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface VolumeChangeEventRepository extends CrudRepository<VolumeChangeEventEntity, Long> {

    <S extends VolumeChangeEventEntity> S save(S s);

    Iterable<VolumeChangeEventEntity> findAllById(Iterable<Long> iterable);

    Optional<VolumeChangeEventEntity> findById(Long aLong);

    void delete(VolumeChangeEventEntity event);

    void deleteAll();

    void deleteById(Long aLong);

    Iterable<VolumeChangeEventEntity> findAll();
}
