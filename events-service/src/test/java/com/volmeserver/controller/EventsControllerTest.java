package com.volmeserver.controller;

import com.volmeserver.dto.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.rest.request.VehicleVolumeChangeEvent;
import com.volmeserver.dto.rest.response.BaseServerResponse;
import com.volmeserver.dto.rest.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.service.VolumeChangeEventServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class EventsControllerTest {
    @Mock
    private VolumeChangeEventServiceImpl volumeChangeEventService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Clock clock;

    @InjectMocks
    private EventsController controller;

    @Test
    public void shouldReceiveEventAndPassItToService() {
        //given
        VehicleVolumeChangeEvent event = new VehicleVolumeChangeEvent("some-event", 1L,
                "some-location", "1550058154424");
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(1550058154424L));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault());
        ResponseEntity<BaseServerResponse> expected = new ResponseEntity<>(
                new VehicleVolumeChangeEventResponse(
                String.format("\n The following event has been successfully saved: " +
                        "VehicleVolumeChangeEvent{event_type='some-event', device_id='1, " +
                        "location='some-location', date='%s}\n", date.toString()), LocalDateTime.now(clock))
                , HttpStatus.OK);
        doNothing().when(volumeChangeEventService).processEvent(event);
        //when
        controller.receiveVolumeChangeEvent(event);
        //then
        verify(volumeChangeEventService, times(1)).processEvent(event);
    }

    @Test
    public void shouldReturnEventResponseById() {
        //given
        Long id = 1L;
        ResponseEntity<VolumeChangeEventEntityResponse> expected = new ResponseEntity<>(
                new VolumeChangeEventEntityResponse(
                        id, "some-event", 1L, "some-location",
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())),
                HttpStatus.OK);
        VolumeChangeEventEntityResponse entityResponse = new VolumeChangeEventEntityResponse(
                id, "some-event", 1L, "some-location",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault()));
        when(volumeChangeEventService.getEvent(id)).thenReturn(entityResponse);
        //when
        ResponseEntity<VolumeChangeEventEntityResponse> actual = controller.getEvent(id);
        //then
        verify(volumeChangeEventService, times(1)).getEvent(id);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldReturnAllEvents() {
        //given
        ResponseEntity<List<VolumeChangeEventEntityResponse>> expected = new ResponseEntity<>(
                Arrays.asList(
                        new VolumeChangeEventEntityResponse(
                                1L, "some-event", 1L, "some-location",
                                LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())),

                        new VolumeChangeEventEntityResponse(
                                2L, "another-event", 2L, "another-location",
                                LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())),

                        new VolumeChangeEventEntityResponse(3L,
                                "and-another-one", 3L, "and-another-one-location",
                                LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault()))),
                HttpStatus.OK);

        List<VolumeChangeEventEntityResponse> events = Arrays.asList(
                new VolumeChangeEventEntityResponse(
                        1L, "some-event", 1L, "some-location",
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())),
                new VolumeChangeEventEntityResponse(
                        2L, "another-event", 2L, "another-location",
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())),
                new VolumeChangeEventEntityResponse(
                        3L, "and-another-one", 3L, "and-another-one-location",
                        LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault())));
        when(volumeChangeEventService.getAllEvents()).thenReturn(events);
        //when
        ResponseEntity<List<VolumeChangeEventEntityResponse>> actual = controller.getAllEvents();
        //then
        verify(volumeChangeEventService, times(1)).getAllEvents();
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldDeleteEventById() {
        //given
        Long id = 1927L;
        doNothing().when(volumeChangeEventService).deleteEvent(id);
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(1550058154424L));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        //when
        controller.deleteEventById(id);
        //then
        verify(volumeChangeEventService, times(1)).deleteEvent(id);
    }


    @Test
    public void shouldDeleteAllEvents() {
        //given
        doNothing().when(volumeChangeEventService).deleteAllEvents();
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(1550058154424L));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        //when
        controller.deleteAllEvents();
        //then
        verify(volumeChangeEventService, times(1)).deleteAllEvents();
    }

    @Test
    public void doesExit() {
        //given
        Long id = 1L;
        VehicleVolumeChangeEventResponse serviceResponse = new VehicleVolumeChangeEventResponse(
                "The entity with the given id exits!",
                LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault()));
        ResponseEntity<VehicleVolumeChangeEventResponse> expected = new ResponseEntity<>(
                serviceResponse, HttpStatus.OK);
        when(volumeChangeEventService.doesExist(id)).thenReturn(serviceResponse);
        //when
        ResponseEntity<BaseServerResponse> actual = controller.doesExits(id);
        //then
        verify(volumeChangeEventService, times(1)).doesExist(id);
        assertThat(actual, is(equalTo(expected)));

    }
}