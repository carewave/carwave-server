package com.volmeserver.service;

import com.volmeserver.dao.VolumeChangeEventDaoImpl;
import com.volmeserver.dto.domain.VolumeChangeEventEntityResponse;
import com.volmeserver.dto.rest.request.Pair;
import com.volmeserver.dto.rest.request.VehicleVolumeChangeEvent;
import com.volmeserver.dto.rest.response.VehicleVolumeChangeEventResponse;
import com.volmeserver.feign.PairingServiceClient;
import com.volmeserver.model.VolumeChangeEventEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class VolumeChangeEventServiceImplTest {
    private static final Clock CLOCK = Clock.fixed(Instant.ofEpochMilli(1550058154424L), ZoneId.systemDefault());

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Clock clock;

    @Mock
    private VolumeChangeEventDaoImpl eventDto;

    @Mock
    private PairingServiceClient pairingServiceClient;

    @Mock
    private NotificationService notificationService;

    @InjectMocks
    private VolumeChangeEventServiceImpl volumeChangeEventService;

    @Test
    public void shouldSaveEvent() {
        //given
        VolumeChangeEventEntity entity = new VolumeChangeEventEntity("some-event", 1927L,
                "Kyiv", LocalDateTime.now(CLOCK));
        when(eventDto.save(entity)).thenReturn(entity);
        VehicleVolumeChangeEvent event = new VehicleVolumeChangeEvent(
                "some-event", 1927L, "Kyiv", "1550058154424");
        List<Pair> expectedPairs = Arrays.asList(
                new Pair("email1@gmail.com", "1927", null,
                        null, true),
                new Pair("email2@gmail.com", "1927", null,
                        null, true));
        when(pairingServiceClient.getPairsByDeviceId("1927")).thenReturn(expectedPairs);
        doNothing().when(notificationService).notifyUsers("some-event", expectedPairs);
        //when
        volumeChangeEventService.processEvent(event);
        //then
        verify(eventDto, times(1)).save(entity);
        verify(pairingServiceClient, times(1)).getPairsByDeviceId("1927");
        verify(notificationService, times(1)).notifyUsers("some-event", expectedPairs);
    }

    @Test
    public void shouldDeleteAllEvents() {
        //given
        doNothing().when(eventDto).deleteAll();
        //when
        volumeChangeEventService.deleteAllEvents();
        //than
        verify(eventDto, times(1)).deleteAll();
    }

    @Test
    public void shouldReturnEventById() {
        //given
        Long id = 1L;
        VolumeChangeEventEntity repositoryResponse = new VolumeChangeEventEntity("some_event",
                1927L, "Kyiv", LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L),
                ZoneId.systemDefault()));
        repositoryResponse.setId(id);
        when(eventDto.findById(id)).thenReturn(repositoryResponse);
        VolumeChangeEventEntityResponse expected = new VolumeChangeEventEntityResponse(1L, "some_event",
                1927L, "Kyiv", LocalDateTime.now(CLOCK));
        //when
        VolumeChangeEventEntityResponse actual = volumeChangeEventService.getEvent(id);
        //then
        verify(eventDto, times(1)).findById(1L);
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldReturnListOfEventsByIds() {
        //given
        List<Long> ids = Arrays.asList(1L, 2L, 1927L);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L),
                ZoneId.systemDefault());
        List<VolumeChangeEventEntity> repositoryResponse = Arrays.asList(
                new VolumeChangeEventEntity(
                        "some-event", 1L, "never-land", localDateTime),
                new VolumeChangeEventEntity(
                        "another-event", 2L, "Kyiv", localDateTime),
                new VolumeChangeEventEntity(
                        "and-another-one", 3L, "somewhere", localDateTime)
        );
        repositoryResponse.get(0).setId(1L);
        repositoryResponse.get(1).setId(2L);
        repositoryResponse.get(2).setId(1927L);
        when(eventDto.findAllById(ids)).thenReturn(repositoryResponse);
        List<VolumeChangeEventEntityResponse> expected = Arrays.asList(
                new VolumeChangeEventEntityResponse(
                        1L, "some-event", 1L, "never-land", localDateTime),
                new VolumeChangeEventEntityResponse(
                        2L, "another-event", 2L, "Kyiv", localDateTime),
                new VolumeChangeEventEntityResponse(
                        1927L, "and-another-one", 3L, "somewhere", localDateTime)
        );
        //when
        List<VolumeChangeEventEntityResponse> actual = volumeChangeEventService.getEvents(ids);
        //then
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldReturnAllEvents() {
        //given
        List<Long> ids = Arrays.asList(1L, 2L, 1927L);
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(1550058154424L),
                ZoneId.systemDefault());
        List<VolumeChangeEventEntity> repositoryResponse = Arrays.asList(
                new VolumeChangeEventEntity(
                        "some-event", 1L, "never-land", localDateTime),
                new VolumeChangeEventEntity(
                        "another-event", 2L, "Kyiv", localDateTime),
                new VolumeChangeEventEntity(
                        "and-another-one", 3L, "somewhere", localDateTime)
        );
        repositoryResponse.get(0).setId(1L);
        repositoryResponse.get(1).setId(2L);
        repositoryResponse.get(2).setId(1927L);
        when(eventDto.findAll()).thenReturn(repositoryResponse);
        List<VolumeChangeEventEntityResponse> expected = Arrays.asList(
                new VolumeChangeEventEntityResponse(
                        1L, "some-event", 1L, "never-land", localDateTime),
                new VolumeChangeEventEntityResponse(
                        2L, "another-event", 2L, "Kyiv", localDateTime),
                new VolumeChangeEventEntityResponse(
                        1927L, "and-another-one", 3L, "somewhere", localDateTime)
        );
        //when
        List<VolumeChangeEventEntityResponse> actual = volumeChangeEventService.getAllEvents();
        //then
        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void shouldDeleteEventById() {
        //given
        Long id = 1927L;
        doNothing().when(eventDto).deleteById(id);
        //when
        volumeChangeEventService.deleteEvent(id);
        // then
        verify(eventDto, times(1)).deleteById(id);
    }

    @Test
    public void shouldValidateIfEventExists() {
        //given
        Long id = 1927L;
        when(eventDto.doesExist(id)).thenReturn(true);
        when(clock.instant()).thenReturn(Instant.ofEpochMilli(1550058154424L));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        VehicleVolumeChangeEventResponse expected = new VehicleVolumeChangeEventResponse(
                "The event exists in the database", LocalDateTime.now(CLOCK));
        //when
        VehicleVolumeChangeEventResponse actual = volumeChangeEventService.doesExist(id);
        //then
        verify(eventDto, times(1)).doesExist(id);
        assertThat(actual, is(equalTo(expected)));
    }

}