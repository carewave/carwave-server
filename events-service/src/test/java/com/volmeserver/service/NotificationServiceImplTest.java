package com.volmeserver.service;

import com.volmeserver.dto.rest.request.Pair;
import com.volmeserver.dto.rest.request.PushNotificationRequest;
import com.volmeserver.dto.rest.response.PushNotificationResponse;
import com.volmeserver.exception.NotificationException;
import com.volmeserver.feign.NotificationServiceClient;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class NotificationServiceImplTest {

    @Mock
    private NotificationServiceClient notificationServiceClient;

    @InjectMocks
    private NotificationServiceImpl subject;

    @Rule
    public ExpectedException exception = ExpectedException.none();


    @Test
    public void notifyUsers_shouldCallNotificationClientMethodWithCorrectRequestParameter() {
        //given
        String eventType = "penetration";
        String fcmPushNotificationToken = "1927token1902";
        PushNotificationRequest request = new PushNotificationRequest(
                "!VEHICLE PENETRATION ALERT!",
                "Someone is inside your car - click here for a live stream!",
                "default_topic", "default_condition", fcmPushNotificationToken);
        PushNotificationResponse expected = new PushNotificationResponse(
                200, "Notification has been sent.");
        List<Pair> pairs = Collections.singletonList(
                new Pair(
                        "email", "device_id", fcmPushNotificationToken,
                        "00:00:00:00:00:00", true));
        when(notificationServiceClient.sendNotificationToDevice(request)).thenReturn(expected);

        //when
        subject.notifyUsers(eventType, pairs);

        //then
        verify(notificationServiceClient, times(1)).sendNotificationToDevice(request);
    }

    @Test
    public void notifyUsers_shouldCallNotificationClientMethodWithCorrectRequestParameterForMultiplePairs() {
        //given
        String eventType = "penetration";
        String fcmPushNotificationToken1 = "1927token1902";
        PushNotificationRequest request1 = new PushNotificationRequest(
                "!VEHICLE PENETRATION ALERT!",
                "Someone is inside your car - click here for a live stream!",
                "default_topic", "default_condition", fcmPushNotificationToken1);
        PushNotificationResponse expected1 = new PushNotificationResponse(
                200, "Notification has been sent.");
        String fcmPushNotificationToken2 = "token1902";
        PushNotificationRequest request2 = new PushNotificationRequest(
                "!VEHICLE PENETRATION ALERT!",
                "Someone is inside your car - click here for a live stream!",
                "default_topic", "default_condition", fcmPushNotificationToken2);
        PushNotificationResponse expected2 = new PushNotificationResponse(
                200, "Notification has been sent.");
        List<Pair> pairs = Arrays.asList(
                new Pair(
                        "email", "device_id", fcmPushNotificationToken1,
                        "00:00:00:00:00:00", true),
                new Pair("email", "device_id", fcmPushNotificationToken2,
                        "00:00:00:00:00:00", true));
        when(notificationServiceClient.sendNotificationToDevice(request1)).thenReturn(expected1);
        when(notificationServiceClient.sendNotificationToDevice(request2)).thenReturn(expected2);

        //when
        subject.notifyUsers(eventType, pairs);

        //then
        verify(notificationServiceClient, times(1)).sendNotificationToDevice(request1);
        verify(notificationServiceClient, times(1)).sendNotificationToDevice(request1);
    }

    @Test
    public void notifyUsers_shouldThrowNotificationExceptionWithCorrectMessageIfResponseNot200() {
        //given
        String eventType = "penetration";
        String fcmPushNotificationToken = "1927token1902";
        PushNotificationRequest request = new PushNotificationRequest(
                "!VEHICLE PENETRATION ALERT!",
                "Someone is inside your car - click here for a live stream!",
                "default_topic", "default_condition", fcmPushNotificationToken);
        PushNotificationResponse response = new PushNotificationResponse(
                500, "Notification sending failed!");
        List<Pair> pairs = Collections.singletonList(
                new Pair(
                        "email", "device_id", fcmPushNotificationToken,
                        "00:00:00:00:00:00", true));
        when(notificationServiceClient.sendNotificationToDevice(request)).thenReturn(response);
        exception.expect(NotificationException.class);
        exception.expectMessage("A problem occurred during notification sending: status code - 500, " +
                "message - Notification sending failed!");

        //when
        subject.notifyUsers(eventType, pairs);

        //then NotificationException thrown
    }

    @Test
    public void notifyUsers_shouldThrowNotificationExceptionWithCorrectMessageIfEventTypeNotFound() {
        //given
        String eventType = "wrong-event-type";
        String fcmPushNotificationToken = "1927token1902";
        PushNotificationRequest request = new PushNotificationRequest(
                "!VEHICLE PENETRATION ALERT!",
                "Someone is inside your car - click here for a live stream!",
                "default_topic", "default_condition", fcmPushNotificationToken);
        PushNotificationResponse response = new PushNotificationResponse(
                500, "Notification sending failed!");
        List<Pair> pairs = Collections.singletonList(
                new Pair(
                        "email", "device_id", fcmPushNotificationToken,
                        "00:00:00:00:00:00", true));
        when(notificationServiceClient.sendNotificationToDevice(request)).thenReturn(response);
        exception.expect(NotificationException.class);
        exception.expectMessage(
                "A problem occurred during notification sending: unsupported event type: wrong-event-type");

        //when
        subject.notifyUsers(eventType, pairs);

        //then NotificationException thrown
    }
}