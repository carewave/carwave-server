                                                **Volme EVENTS-SERVICE**
                                                
On the current state of the project EVENTS-SERVICE is nothing more than a crud application for storing events objects.

It requires a database container, the command for instantiating it is: 
docker run --name volme_events_server -d -p 1969:3306 -e MYSQL_DATABASE=volme -e MYSQL_ROOT_PASSWORD=password mysql:5.7
