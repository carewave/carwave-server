                                                **Volme REGISTRY SERVICE**
                                                
This service is a classic 'registry' service from Spring Cloud infrastructure. It has Eureka server under the hood and 
registers all the existing services in the project to provide 'routing' functionality.  